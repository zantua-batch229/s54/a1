import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext.js';
import { Navigate } from 'react-router-dom';


export default function Login(){

	const {user, setUser} = useContext(UserContext);

	const [userName, setUserName] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);

	useEffect(() => {

		if(userName !== '' && password !== ''){
			setIsActive(true);
		} else {
			setIsActive(false);
		}


	})

	function Login(e){
		e.preventDefault();

		// set the email of the authenticated user in the local storage
		/*
		Syntax : localStorage.seetItem('propertyName', value)
		*/
		localStorage.setItem('email', userName);

		setUser({
			email: localStorage.getItem('email')
		})

		setUserName('');
		setPassword('');
		setIsActive(false);
		alert(`Welcome ${userName}!`);
	}

	return(
			
			(user.email !== null) 
			?
				<Navigate to="/courses"/>
			:


				
				
				<Form onSubmit={(e) => Login(e)}>
					<h1>LOGIN</h1>

					<Form.Group className="mb-3" id="userName">
						<Form.Label>User Name</Form.Label>
						{/*Form control is self-closing. It is where the values are set*/}
						<Form.Control 
							type="email" 
							placeholder="Email" 
							required 
							value={userName} 
							onChange={e => setUserName(e.target.value)}/>
					</Form.Group>

					<Form.Group className="mb-3" id="password">
						<Form.Label>Password</Form.Label>
						<Form.Control 
							type="password" 
							placeholder="Password" 
							required 
							value={password} 
							onChange={e => setPassword(e.target.value)}/>
					</Form.Group>

					
					{
						isActive 
						?
						<Button variant="success" type="submit" id="loginBtn">
			        	Login
			      		</Button>
			      		:
			      		<Button variant="success" type="submit" id="loginBtn" disabled>
			        	Login
			      		</Button>

		      		}
		      		

				</Form>




		)
}